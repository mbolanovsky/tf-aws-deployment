# Master
resource "aws_instance" "master" {
  count                  = 1
  ami                    = var.image
  instance_type          = var.instance
  subnet_id              = aws_subnet.dmz.id
  vpc_security_group_ids = [aws_security_group.base.id]
  private_ip             = "10.0.1.69"
  user_data              = file("postinstall-m.sh")
# user_data = "${templatefile("${path.module}/bootstrap.tmpl",{BACKUP = ${var.BACKUP}})}"

  tags = {
    Name  = "master${count.index + 1}"
    Owner = var.owner
  }
}

# Workers
resource "aws_instance" "worker" {
  count                  = 3
  ami                    = var.image
  instance_type          = var.instance
  subnet_id              = aws_subnet.dmz.id
  vpc_security_group_ids = [aws_security_group.base.id]
  user_data              = file("postinstall-w.sh")

  tags = {
    Name  = "worker${count.index + 1}"
    Owner = var.owner
  }
}
